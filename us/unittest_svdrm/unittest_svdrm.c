#include "svdrmDummy.h"
#include "unity.h"




void test_setDeviceInformation(void)
{
	/* Test for correct handling of invalid file descriptors */
	TEST_ASSERT_EQUAL(NULL, getVersionInformation(0));
	TEST_ASSERT_EQUAL(NULL, getVersionInformation(-1));
	TEST_ASSERT_EQUAL(NULL, getVersionInformation(-100));

}
void test_registerDevice(void)
{
	/* Test for correct handling of invalid file descriptors */
	TEST_ASSERT_EQUAL(1, registerDevice(0, NULL));
    TEST_ASSERT_EQUAL(1, registerDevice(-1, NULL));
    TEST_ASSERT_EQUAL(1, registerDevice(-10, NULL));
    TEST_ASSERT_EQUAL(1, registerDevice(-1000, NULL));
}

