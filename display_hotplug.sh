#!/bin/sh

# Query with xrandr to get the list of available modes
# Since this script is called from the udev rule, 10-openxtfb.rules, it will be run when a new mode has been added
# Run xrandr a second time, X seems to provide more information on new modes during the second query
# Then switch to the newly detected, preferred mode using the --auto flag
xrandr -display :0 --query > /dev/null 2>&1
xrandr -display :0 --query > /dev/null 2>&1

for i in $(xrandr -display :0  | grep " connected" | awk '{print $1}'); do
    xrandr -display :0 --output $i --auto
done

sleep 1
touch /tmp/svdrm-hotplug
